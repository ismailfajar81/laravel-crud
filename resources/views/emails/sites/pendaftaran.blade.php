@component('mail::message')
# Pendaftaran Siswa

Selamat anda telah terdaftar di MI Baiturrahim.

@component('mail::button', ['url' => 'http://ismailfajar.com'])
Klik di sini
@endcomponent

Terima kasih,<br>
{{ config('app.name') }}
@endcomponent
