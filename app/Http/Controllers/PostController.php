<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Redirect;

use function GuzzleHttp\Promise\all;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('posts.index', compact(['posts']));
    }

    public function add()
    {
        return view('posts.add');
    }
    
    public function create(Request $request)
    {
        
        $posts = Post::create([
            
            'title' => $request->title,
            'content' => $request->content,
            'user_id' => auth()->user()->id,
            'thumbnail' => $request->thumbnail
        ]);
        return Redirect::to('posts')->with('sukses', 'Post berhasil disubmit');
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts/edit', ['post'=> $post]);
    }

    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'user_id' => 'required',
            'thumbnail' => 'required'
        ]);
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->user_id = auth()->user()->id;
        $post->thumbnail = $request->input('thumbnail');
        $post->save();

        return Redirect::to('posts')->with('sukses', 'Data berhasil diupdate');
    }

    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('posts')->with('sukses', 'Data Berhasil dihapus');
    }
}
