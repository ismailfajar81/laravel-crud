<?php

namespace App\Http\Controllers;

use App\Mail\notifPendaftaranSiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Post;
use Illuminate\Support\Facades\Mail;
use App\User;

class SiteController extends Controller
{
    public function home()
    {
        $posts = Post::all();
        return view('sites.home', compact(['posts']));
    }

    public function register()
    {
        return view ('sites.register');
    }

    public function postregister(Request $request)
    {
        $user = new \App\User;
        $user->role='siswa';
        $user->name = $request->nama_depan;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->remember_token = Str::random(60);
        $user->save();

        $request->request->add(['user_id' => $user->id]);

        Mail::to($user->email)->send(new notifPendaftaranSiswa);

        return redirect ('/')->with('sukses', 'Terima kasih sudah mendaftar');
    }

    public function singlepost($slug)
    {
        $post = Post::where('slug', '=' , $slug)->first();
        return view('sites.singlepost', compact(['post']));
    }
}
